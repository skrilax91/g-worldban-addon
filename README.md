# G-WorldBan

G-WorldBan reprend le système de source ban de steam. G-worldban va enregistrer dans nos infrastructures les bannissements permanent ou SNTE de votre serveur et de tout les serveurs l'utilisant.
De votre coté vous pouvez, grasse au panel, autoriser les joueurs ne possédant qu'un certain nombre de bannissement, ou interdire les joueurs ayants eu des bannissement SNTE.

## Installation

Rendez-vous sur le [panel de G-Worldban](http://gwb.dragibull.fr/) pour créer un serveur et télécharger l'addon.
(/!\ N'utilisez pas les fichiers de Gitlab, ils peuvent ne pas être à jour !).

## Utilisation

G-WorldBan est simple d'utilisation, il suffit simplement de créer un serveur sur le panel, puis de télécharger l'addon.
Vous y trouverez un fichier config_file.lua où se trouvent les config.

## Contribuer
Les Pull requests sont les bienvenus. Pour les changements majeurs, veuillez d'abord ouvrir une Issue pour discuter de ce que vous souhaitez changer.

## Support

Tout support à propos de G-WorldBan ce fait sur le panel dans la section support

## Remerciement

Je remercie :
- **SlownLS** : pour avoir créer le panel en nodeJS
- **Nills & Maks** : pour avoir fournis des informations sur les vulnérabilités du panel
- **Surevil** : pour avoir patch l'incompatibilité avec G-WorldBan et CPE - Anti Backdoor
- L'ensemble des utilisateurs de G-WorldBan