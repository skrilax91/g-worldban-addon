local lang = {}

lang.error = {
	["100"] = [[[GWB][ERROR] Looks like your server isn't registered on our infrastructures or that your files of configuration is wrong. Check the validity of your configuration as well as the status of your panel here :http://www.dragibull.fr/GWB/]],
	["303"] = "[GWB][UPDATER] GWorldBan needs to be updated, for security purposes G-WorldBan won't be initialized.\n\n",
	["500"] = "[GWB][ERROR] There's an Update of the G-WorldBan servers , to avoid any problems your add-on won't be initialized !\n\n",
	["404"] = "Oops ! Looks like there's something wrong, check your configuration file!\n\n",


	["singleplayer"] = "[GWB][INFO] You are running G-WorldBan in solo play, this add-on is only compatible with dedicated servers !",
	["notdedicated"] = "[GWB][INFO] You are running G-WorldBan on a non-dedicated server, this add-on is only compatible with dedicated servers !",
	['noconfig'] = "[GWB][ERROR] You need a configuration file ! For more details, go check : http://www.dragibull.fr/GWB/\n\n",
	["noadminmod"] = '[GWB] No moderation add-on found ! GWorldBan is currently working with ULib/ULX, Serverguard and FAdmin. Without, G-WorldBan can\'t synchronize the bans\n',
	["APIError"] = "[GWB] : An Error Occured with the Api : <errormess>",
}

lang.info = {
	["loaded"] = "[GWB] G-WorldBan is initialized !\n\n",
	["bancount"] = "[GWB] You currently have <bans> bans within our database",
}

lang.debug = {
	["authalre"] = "[GWB] : Authentification already engaged",
	["auth"] = "[GWB] : Authentification with the servers of G-WorldBan",
	["added"] = "[GWB] : Player added on the table of connected players",
	["errorsess"] = "[GWB] : Invalid game session, reconnection request for the player <player>",
}

lang.bans = {
	["bans"] = "GWorldBan is activated on this server, and it seems that you have too many ban",
	["snte"] = "GWorldBan is activated on this server, and it seems that you are banned by SNTE",
	["GS"] = "GWorldBan is activated on this server, and it seems that you are banned by GSecure",
}

return lang