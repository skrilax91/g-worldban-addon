local lang = {}

lang.error = {
	["100"] = [[[GWB][ERROR] Il semblerais que votre serveur ne sois pas enregistré sur nos infrastructures ou que votre fichier de configuration soit éroné. Veuillez vérifier la validité de votre configuration ansi que le statut de votre serveur sur notre panel ici :http://www.dragibull.fr/GWB/]],
	["303"] = "[GWB][UPDATER] GWorldBan a besoins d'être mis à jour, pour des questions de sécurité G-WorldBan ne serra pas initialisé.\n\n",
	["500"] = "[GWB][ERROR] Une maintenance des serveurs G-WorldBan est en cours, pour éviter tout problème votre add-on ne serra pas initialisé !\n\n",
	["404"] = "Oops ! Il semblerais que quelque chose ne va pas, vérifiez votre fichier de configuration !\n\n",


	["singleplayer"] = "[GWB][INFO] Vous faite tourner G-WorldBan en solo, cet add-on est uniquement compatible avec les serveurs dédiés !",
	["notdedicated"] = "[GWB][INFO] Vous faite tourner G-WorldBan sur un server non dédié, cet add-on est uniquement compatible avec les serveurs dédiés !",
	['noconfig'] = "[GWB][ERROR] Vous avez besoin d'un fichier de configuration ! Pour plus de détails : http://www.dragibull.fr/GWB/\n\n",
	["noadminmod"] = '[GWB] Aucun addon de modération compatible trouvé ! GWorldBan est actuellement compatible avec ULib/ULX, Serverguard et FAdmin. Sans, G-WorldBan ne peux pas synchroniser les bans\n',
	["APIError"] = "[GWB] : Une erreur s'est produite avec l'Api : <errormess>",
}

lang.info = {
	["loaded"] = "[GWB] G-WorldBan est initialisé !\n\n",
	["bancount"] = "[GWB] Vous avez actuellement <bans> bans au sein de notre database",
}

lang.debug = {
	["authalre"] = "[GWB] : Authentification déjà engagée",
	["auth"] = "[GWB] : Authentification auprès des serveurs de G-WorldBan",
	["added"] = "[GWB] : Joueur ajouté au tableau des joueurs connectés",
	["errorsess"] = "[GWB] : Session de jeu incorrecte, demande de reconnexion pour le joueur <player>",
}

lang.bans = {
	["bans"] = "GWorldBan is activated on this server, and it seems that you have too many ban",
	["snte"] = "GWorldBan is activated on this server, and it seems that you are banned by SNTE",
	["GS"] = "GWorldBan is activated on this server, and it seems that you are banned by GSecure",
}

return lang