--[[INIT FILE -----------------------------------------------------------------------------

$$$$$$\  $$\      $$\                     $$\       $$\ $$$$$$$\                      
$$  __$$\ $$ | $\  $$ |                    $$ |      $$ |$$  __$$\                     
$$ /  \__|$$ |$$$\ $$ | $$$$$$\   $$$$$$\  $$ | $$$$$$$ |$$ |  $$ | $$$$$$\  $$$$$$$\  
$$ |$$$$\ $$ $$ $$\$$ |$$  __$$\ $$  __$$\ $$ |$$  __$$ |$$$$$$$\ | \____$$\ $$  __$$\ 
$$ |\_$$ |$$$$  _$$$$ |$$ /  $$ |$$ |  \__|$$ |$$ /  $$ |$$  __$$\  $$$$$$$ |$$ |  $$ |
$$ |  $$ |$$$  / \$$$ |$$ |  $$ |$$ |      $$ |$$ |  $$ |$$ |  $$ |$$  __$$ |$$ |  $$ |
\$$$$$$  |$$  /   \$$ |\$$$$$$  |$$ |      $$ |\$$$$$$$ |$$$$$$$  |\$$$$$$$ |$$ |  $$ |
 \______/ \__/     \__| \______/ \__|      \__| \_______|\_______/  \_______|\__|  \__|


-----------------------------------------------------------------------------------------]]
GWBConfig = {}
GWBan = {}
include("gworldban/gwutils/sh_notifications.lua")

local function retrieveLang()
	if file.Exists("config/lang/"..GWBConfig.settings.lang..".lua", "LUA" ) then
		GWBConfig.lang = include("config/lang/"..GWBConfig.settings.lang..".lua")
		return true
	else
		print("[GWB] : Your lang file is not recognized, 'en' is set")
		if !file.Exists("config/lang/en.lua", "LUA" ) then
			print("[GWB] : 'en' can't be load, G-WorldBan can't init")
			return false
		else
			GWBConfig.lang = include("config/lang/en.lua")
		end
	end
	return true
end

local port = string.Explode( ":", game.GetIPAddress() )[2]
local function CheckLicence()
	http.Post(GWBConfig.api.url.."check/", {key = GWBConfig.api.key, version = "0.1.0", port = port} , function(b)
		local body = util.JSONToTable(b)
	    -- Le corps réceptionné est nil, erreur
	    if body == nil then
	      return
	    end
	    

	    -- Une erreur s'est produite lors du traitement sur l'API
	    if body["error"] then
	      MsgC(Color(255,0,0), string.gsub( GWBConfig.lang.error["APIError"].."\n", '<errormess>', GWBConfig.lang.error[tostring(body["code"])] or tostring(body["error"])))
	      return
	    end

	    if body["code"] == 200 then
	    	print("[GWB] "..body["response"])
	    	concommand.Remove('gwb_init')
			include("gworldban/gworldban.lua")
		end
	end, function(err) print("error") MsgC(Color(255,0,0), string.gsub( GWBConfig.lang.error["APIError"].."\n", '<errormess>', err)) end)
end
local text = [[

$$$$$$\  $$\      $$\                     $$\       $$\ $$$$$$$\                      
$$  __$$\ $$ | $\  $$ |                    $$ |      $$ |$$  __$$\                     
$$ /  \__|$$ |$$$\ $$ | $$$$$$\   $$$$$$\  $$ | $$$$$$$ |$$ |  $$ | $$$$$$\  $$$$$$$\  
$$ |$$$$\ $$ $$ $$\$$ |$$  __$$\ $$  __$$\ $$ |$$  __$$ |$$$$$$$\ | \____$$\ $$  __$$\ 
$$ |\_$$ |$$$$  _$$$$ |$$ /  $$ |$$ |  \__|$$ |$$ /  $$ |$$  __$$\  $$$$$$$ |$$ |  $$ |
$$ |  $$ |$$$  / \$$$ |$$ |  $$ |$$ |      $$ |$$ |  $$ |$$ |  $$ |$$  __$$ |$$ |  $$ |
\$$$$$$  |$$  /   \$$ |\$$$$$$  |$$ |      $$ |\$$$$$$$ |$$$$$$$  |\$$$$$$$ |$$ |  $$ |
 \______/ \__/     \__| \______/ \__|      \__| \_______|\_______/  \_______|\__|  \__|


]]
local function gwb_init()
	MsgC (Color( 210, 105, 30 ), text)
	if !file.Exists("config/config_file.lua", "LUA" ) then
		print("Config File missing !")
		return
	end
	GWBConfig = include("config/config_file.lua")
	if !GWBConfig then
		print("Config file is corrupted!")
		return
	end
	if not retrieveLang() then return end
	if game.SinglePlayer() then MsgC (Color( 255, 155, 255 ), GWBConfig.lang.error["singleplayer"]) return end
	if not game.IsDedicated() then MsgC (Color( 255, 155, 255 ), GWBConfig.lang.error["notdedicated"]) return end
	CheckLicence()
end

--Add initialization console command

hook.Add("Think", "GWBInit", function()
	concommand.Add('gwb_init', gwb_init, nil, 'Initialize GWorldBan', FCVAR_SERVER_CAN_EXECUTE)
	gwb_init()
	hook.Remove("Think", "GWBInit")
end)