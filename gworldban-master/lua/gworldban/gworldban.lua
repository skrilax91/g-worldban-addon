include('gworldban/gwutils/sv_utils.lua')
include('gworldban/api/api.lua')

if ULib then
	include('gworldban/bans_systems/ulx.lua')
elseif serverguard then
	include('gworldban/bans_systems/serverguard.lua')
elseif engine.ActiveGamemode() == "darkrp" or FAdmin then
	include('gworldban/bans_systems/fadmin.lua')
else
	ErrorNoHalt(GWBConfig.lang.error["404"])
end

--[[---------------------------------------------------------------------------
Hooks catched
---------------------------------------------------------------------------]]
hook.Add("PlayerAuthed", "GWB_PlayerAuthed", function(ply, steamid, uniqueid)
  -- Timer added to avoid FAdmin error NULL Entity
  timer.Simple(3, function() GWBan.checkPlayerConnection(ply) end)
end)

hook.Add("PlayerDisconnected", "GWB_PlayerDiconnection", function(ply)
  GWBan.deleteConnectedPlayer(ply)
end)

print(GWBConfig.lang.info["loaded"])