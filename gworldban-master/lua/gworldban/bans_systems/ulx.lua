local gwboldBan = ULib.addBan
local gwboldUnban = ULib.unban

function ULib.addBan(steamid, time, reason, name, admin)
	local data = {}
	data.duration = (time and time * 60) or 0
	data.reason = reason or "None"
	data.issuer_steamId = (IsValid(admin) and admin:SteamID64() or "0")
	data.subject_steamId = util.SteamIDTo64(steamid)
	data.method = 0
	data.subject_name = name
	if time == 0 then
		gworldban_api.add_ban(data)
	end
	return gwboldBan(steamid, time, reason, name, admin)
end

function ULib.unban(steamid, admin)
	gworldban_api.remove_ban(util.SteamIDTo64(steamid))
	return gwboldUnban(steamid, admin)
end
print("[GWB] Ulx set up")