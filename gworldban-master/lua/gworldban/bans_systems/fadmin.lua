hook.Add("FAdmin_StoreBan", "GWB:FAdmiminSync", function(steamid, nick, duration, reason, adminnick, adminid)

	local data = {}
	data.duration = (duration or 0)
	data.reason = (reason or "None")
	data.issuer_steamId = (adminid and util.SteamIDTo64(adminid) or "0")
	data.subject_steamId = util.SteamIDTo64(steamid)
	data.method = 0
	if data.duration == 0 then
		gworldban_api.add_ban(data)
	end
end)

hook.Add("FAdmin_UnBan", "GWB:FAdminSync2", function(ply, steamid)
	gworldban_api.remove_ban(util.SteamIDTo64(steamid))
end)

return ban_system