local serverguard.gwboldBan = serverguard.BanPlayer
local serverguard.gwboldUnban = serverguard.UnbanPlayer

function serverguard:BanPlayer(admin, ply, length, reason, bKick, bSilent, adminNameOverride, done)
	if not done then
		local data = {}
		data.duration = (length and length * 60 or 0)
		data.reason = reason or "None"
		data.issuer_steamId = (IsValid(admin) and admin:SteamID64()) or "0"
		data.subject_steamId = (type(ply) == "string" and ply or ply:SteamID64())
		data.method = 0
		if data.duration == 0 then
			gworldban_api.add_ban(data)
		end
	end
	return self:gwboldBan(admin, ply, length, reason, bKick, bSilent, adminNameOverride, true)
end

function serverguard:UnbanPlayer(steamid, admin)
	gworldban_api.remove_ban(util.SteamIDTo64(steamid))
	return self:gwboldUnban(steamid, admin)
end