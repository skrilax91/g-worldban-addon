gworldban_api = {}
function gworldban_api.add_ban(data)
	senddata = {
		serverApi = GWBConfig.api.key,
		playersteamid = data.subject_steamId,
		method = 0,
		reason = data.reason,
		admin = data.issuer_steamId,
	}

	http.Post(GWBConfig.api.url .. "addban/", senddata , function(b)
		local body = util.JSONToTable(b)
		if body == nil then
			MsgC(Color(255,0,0), GWBConfig.lang.error["APIError"])
			return
		end
		color = Color(body.r,body.v,body.b)
		MsgC(color,"[GWB] : "..body.response.."\n")
	end)
end

function gworldban_api.remove_ban(id)
	senddata = {
		serverApi = GWBConfig.api.key,
		playersteamid = id,
	}
	http.Post(GWBConfig.api.url .. "removeban/", senddata , function(b)
		local body = util.JSONToTable(b)
		if body == nil then
			MsgC(Color(255,0,0), GWBConfig.lang.error["APIError"])
			return
		end
		color = Color(body.r,body.v,body.b)
		MsgC(color,"[GWB] : "..body.response)
	end)
end