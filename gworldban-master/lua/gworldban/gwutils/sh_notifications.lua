--[[ DRAGIBULL CORE --------------------------------------------------------------------------------

@package     Dragibull - Core
@author      Devix & Dragicull corp.
@build       v0.1.0
@owner       76561198843669553

EN MODIFIANT CE FICHIER -- VOUS ACCEPTEZ QUE LES AUTEURS MENTIONES SI DESSUS NE SERONS PAS
RESPONSSABLES DES PROBLEMES RECONTRE.
--------------------------------------------------------------------------------------------------]]

function GWBan.Notify( ply, str, duration, notifytype)
	if(SERVER) then
		net.Start("GWBan:Notify")
			net.WriteString(notifytype)
			net.WriteString(str)
			net.WriteInt(duration, 16)
		net.Send(ply)
	elseif(CLIENT) then
		if(notifytype == "Error") then
			notification.AddLegacy(str, NOTIFY_ERROR, duration)
		elseif(notifytype == "Generic") then
			notification.AddLegacy(str, NOTIFY_GENERIC, duration)
		end
	end
end

function GWBan.SendMessage( ply, str)
	ply:ChatPrint(str)
end


if(CLIENT) then
	net.Receive("GWBan:Notify", function()
		notifytype = net.ReadString()
		if(notifytype == "Error") then
			notification.AddLegacy(net.ReadString(), NOTIFY_ERROR, net.ReadInt(duration, 16))
		elseif(notifytype == "Generic") then
			notification.AddLegacy(net.ReadString(), NOTIFY_GENERIC, net.ReadInt(duration, 16))
		end
	end)
end