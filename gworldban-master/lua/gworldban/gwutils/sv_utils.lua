--[[-----------------------------------------------------------------------
Process : Check Player validation 
--------------------------------------------------------------------------]]
function GWBan.checkPlayerConnection(ply, shcedule)
  if GWBan.connectedPlayers == nil then
    GWBan.connectedPlayers = {}
  end

  -- Le joueur a déja été enregistré
  if GWBan.getConnectedPlayer(ply) then

    if GWBConfig.settings.debug then
      print(GWBConfig.lang.debug["authalre"])
    end

    return true
  end

  if GWBConfig.settings.debug then
    print(GWBConfig.lang.debug["auth"])
  end

  -- On ajoute le joueur au tableau des joueurs en connexion
  if not GWBan.getConnectedPlayer(ply) && not shcedule then
    table.insert(GWBan.connectedPlayers, {steamid64 = ply:SteamID64(), status = "CONNECTING", timestamp = math.floor(os.time()) - math.floor(ply:TimeConnected())})
  end
  if not ply:IsValid() then return end

  local param = {
    key = GWBConfig.api.key,
    steamid = ply:SteamID64()
  }

    http.Post(GWBConfig.api.url.."playerInfos/", param, function(body)
        local body = util.JSONToTable(body)

        if not ply:IsValid() then return end
        -- Le corps réceptionné est nil, erreur
        if body == nil then
            print(GWBConfig.lang.error["APIError"]) 
            return
        end

        -- Une erreur s'est produite lors du traitement sur l'API
        if body["error"] then
            MsgC(Color(255,0,0), string.gsub( GWBConfig.lang.error["APIError"], '<errormess>', tostring(body["error"])))

            return
        end

        if body["access"] then

            -- On passe son status en connecté
            GWBan.changeConnectedPlayerStatus(ply, "CONNECTED")

            if GWBConfig.settings.debug then
                print(GWBConfig.lang.debug["added"])
            end

            GWBan.SendMessage(ply, string.gsub( GWBConfig.lang.info["bancount"], '<bans>', body["bans"]))
        else
            ply:Kick(GWBConfig.lang.bans[body["errcode"]])
            if GWBConfig.settings.debug then
                print("Player kicked : "..GWBConfig.lang.bans[body["errcode"]])
            end
        end

        end
    )
end

--[[-----------------------------------------------------------------------
Process : Return player if he is in the connected list
--------------------------------------------------------------------------]]
function GWBan.getConnectedPlayer(ply)
  if GWBan.connectedPlayers == nil then
    GWBan.connectedPlayers = {}
  end

  local actualConnectionTimestamp = math.floor(os.time()) - math.floor(ply:TimeConnected())

  for _, player in pairs(GWBan.connectedPlayers) do
    if ply:SteamID64() == player.steamid64 then
      if (actualConnectionTimestamp - player.timestamp) < 0 || (actualConnectionTimestamp - player.timestamp) > 2 then
        -- Dans ce cas, la session de jeu n'est plus valide, le joueur a donc eu un problème de connexion
        if GWBConfig.settings.debug then
          print(string.gsub( GWBConfig.lang.debug["errormess"], '<player>', ply:Nick()))
        end
        GWBan.deleteConnectedPlayer(ply)

        return false
      end
      return player
    end
  end

  return false
end

--[[-----------------------------------------------------------------------
Process : Remove a player from connected list
--------------------------------------------------------------------------]]
function  GWBan.deleteConnectedPlayer(ply)
  for key, player in pairs( GWBan.connectedPlayers) do
    if ply:SteamID64() == player.steamid64 then
      table.remove( GWBan.connectedPlayers, key)
    end
  end

  return false
end

--[[-----------------------------------------------------------------------
Process : Remove a player from connected list
--------------------------------------------------------------------------]]
function  GWBan.changeConnectedPlayerStatus(ply, status)
  for _, player in pairs( GWBan.connectedPlayers) do
    if ply:SteamID64() == player.steamid64 then
      player.status = status
    end
  end

  return false
end